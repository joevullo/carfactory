import java.util.Random;

/**
 * Created by jvullo on 12/11/14.
 */
public abstract class CarFactory {

    //Returns a car.
    public abstract Car getNewCar();

    /**
     * Sets up the factory.
     */
    private void setupCarFactory() {
        //Would do some magic stuff to setup the class.
    }
}
