import java.util.Random;

/**
 * Created by jvullo on 12/11/14.
 */
public class SaabCarFactory extends CarFactory {

    @Override
    public Car getNewCar() {
            return new SaabCar();
    }
}
