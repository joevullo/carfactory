import java.util.Random;

/**
 * Created by jvullo on 12/11/14.
 */
public class PeugeotCarFactory extends CarFactory {

    @Override
    public Car getNewCar() {
            return new PeugeotCar();
    }
}
