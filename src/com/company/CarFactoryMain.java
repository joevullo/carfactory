import java.util.Scanner;

public class CarFactoryMain {

    public static void main(String[] args) {
       Scanner userInput = new Scanner(System.in);
       Car a = null;
       System.out.println("What car would you like to produce? |FORD|PEUGEOT|SAAB");

       CarFactory myCarFactory;

       if (userInput.hasNextLine()) {
            String result = userInput.nextLine();

           if (result.equalsIgnoreCase("FORD")) {
               myCarFactory = new FordCarFactory();
           } else if (result.equalsIgnoreCase("PEUGEOT")) {
               myCarFactory = new PeugeotCarFactory();
           } else if (result.equalsIgnoreCase("SAAB")) {
               myCarFactory = new SaabCarFactory();
           } else {
               System.out.println("The wrong option was entered (|FORD|PEUGEOT|SAAB) " +
                       "A ford car will now be created.");
               myCarFactory = new FordCarFactory();
           }

           a = myCarFactory.getNewCar();
           displayMenu(a);
        }
    }

    private static void displayMenu(Car car) {
        Scanner userInput = new Scanner(System.in);
        int result = 0;

        System.out.println("\n\n");

        System.out.println("(1) Display MPG");
        System.out.println("(2) Display Top Speed");
        System.out.println("(3) Display Manufacturing Year");
        System.out.println("(4) Exit");

        if (userInput.hasNextInt()) {
            result = userInput.nextInt();
        }

        if (result == 1) {
            car.displayMPG();
        } else if (result == 2) {
            car.displayTopSpeed();
        } else if (result == 3) {
            car.displayManYear();
        } else if (result == 4) {
            return;
        }

        displayMenu(car);
    }
}
