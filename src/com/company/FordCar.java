/**
 * Created by jvullo on 12/11/14.
 */
public class FordCar implements Car {

    @Override
    public void displayMPG() {
        //Unique implementation for displaying Miles Per Gallon would be here.
        System.out.println("This ford car has 35 MPG");
    }

    @Override
    public void displayTopSpeed() {
        //Unique implementation for display top speed would be here.
        System.out.println("The top speed of this ford is 96 MPH");
    }

    @Override
    public void displayManYear() {
        //Unique implementation for diplaying manufacturing year.
        System.out.println("The manufacturing year for this ford was 1992.");
    }
}
