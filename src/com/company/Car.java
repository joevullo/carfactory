/**
 * Created by jvullo on 12/11/14.
 */
public interface Car {

    /**
     * Displays a cars miles per gallon.
     */
    public void displayMPG();

    /**
     * Displays the top speed of a car
     */
    public void displayTopSpeed();

    /**
     * Displays the manufacturing year of the car.
     */
    public void displayManYear();

}
