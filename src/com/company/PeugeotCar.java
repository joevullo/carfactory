/**
 * Created by jvullo on 12/11/14.
 */
public class PeugeotCar implements Car {
    @Override
    public void displayMPG() {
        //Unique implementation for displaying Miles Per Gallon would be here.
        System.out.println("This peugeot car has 15 MPG");
    }

    @Override
    public void displayTopSpeed() {
        //Unique implementation for display top speed would be here.
        System.out.println("The top speed of this peugeot is 160 MPH");
    }

    @Override
    public void displayManYear() {
        //Unique implementation for displaying manufacturing year.
        System.out.println("The manufacturing year for this peugeot was 2001.");
    }
}
